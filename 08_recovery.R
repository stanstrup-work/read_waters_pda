# Confidence interval on estimate -----------------------------------------
# # Chlorogenic acid
# temp <- data_calib %>% 
#                     # filter(into<3e7) %>% 
#                     filter(!is.na(conc)) %>% 
#                     nest(-compound)
# 
# 
# m <- lm(into ~ conc, data = temp$data[[2]])
# 
# # sample 31
# q_s  <- invest(m, 6856099)$estimate  # sample
# q_5  <- invest(m, 7926431)$estimate  # +5ppm
# q_50 <- invest(m, 12069311)$estimate # +50ppm



# Calculate recovery ------------------------------------------------------
# extract the data we want and organize
recovery_data <- quantification %>% 
                    left_join(data_frame(target_idx= c(1,3,10), compound = c("Neochlorogenic acid","Chlorogenic acid","Cyanidin-3-O-glu")), by = "target_idx") %>% 
                    filter(!is.na(compound)) %>% 
                    filter(grepl("_89|_31|_91|sample-89|sample-31|sample-91",files)) %>% 
                    mutate(files = file_path_sans_ext(basename(files))) %>% 
                    select(compound, files, quant_wlm) %>% 
                    mutate(spike_type="unspiked") %>% 
                    mutate(spike_type=ifelse(grepl("+5_",files, fixed = TRUE),"10ppm",spike_type)) %>%
                    mutate(spike_type=ifelse(grepl("+50_",files, fixed = TRUE),"50ppm",spike_type)) %>% 
                    mutate(sample=ifelse(grepl("_31\\+|sample-31",files),31L,NA)) %>% 
                    mutate(sample=ifelse(grepl("_89\\+|sample-89",files),89L,sample)) %>% 
                    mutate(sample=ifelse(grepl("_91\\+|sample-91",files),91L,sample)) %>% 
                    select(compound, sample, spike_type, quant_wlm) %>%
                    group_by(compound, sample, spike_type) %>% 
                    summarise(mean_quant = mean(quant_wlm)) %>% 
                    ungroup() %>% 
                    spread(spike_type, mean_quant) %>% 
                    mutate(unspiked = ifelse(is.na(unspiked),0,unspiked))


recovery_design <- tribble(~compound, ~spike_conc,
                           "Neochlorogenic acid", 147.5,
                           "Chlorogenic acid", 98.5,
                           "Cyanidin-3-O-glu", 105*(449.388/484.84)
                          )


recovery_data <- recovery_data %>% 
                    left_join(recovery_design, by = "compound")

rm(recovery_design)


# recovery calc
recovery_table <- recovery_data %>% 
                    mutate(rec_10ppm = (`10ppm`-unspiked*(180/200))/(spike_conc/10)) %>% 
                    mutate(rec_50ppm = (`50ppm`-unspiked/2)/(spike_conc/2))
    
    
recovery_table_sum <- recovery_table %>% 
    group_by(compound) %>% 
    summarise(rec_10ppm_mean = mean(rec_10ppm),
              # rec_10ppm_CI_lower = rec_10ppm_mean-qt(0.975, n()-1)*sd(rec_10ppm)/sqrt(n()),
              # rec_10ppm_CI_upper = rec_10ppm_mean+qt(0.975, n()-1)*sd(rec_10ppm)/sqrt(n()),
              rec_50ppm_mean = mean(rec_50ppm)#,
              # rec_50ppm_CI_lower = rec_50ppm_mean-qt(0.975, n()-1)*sd(rec_50ppm)/sqrt(n()),
              # rec_50ppm_CI_upper = rec_50ppm_mean+qt(0.975, n()-1)*sd(rec_50ppm)/sqrt(n())
              )


write_xlsx(recovery_table, "result_data/recovery.xlsx")
write_xlsx(recovery_table_sum, "result_data/recovery_sum.xlsx")


# TODO: CI based on pooled SD instead of SD of the averaged data
# Pooled df apparently is n1+n2-2



